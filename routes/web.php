<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Http\Controllers\CrudController;
use App\Http\Controllers\menuController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/Home', [menuController::class, 'index']);

Route::get('/admin', function(){
    return view('admin');
});

Route::get('/crud', 'CrudController@index');
Route::post('/crud/create', 'CrudController@create');
Route::get('/crud/{id}/edit', 'CrudController@edit');
Route::post('/crud/{id}/update', 'CrudController@update');
Route::get('/crud/{id}/delete', 'CrudController@delete');
