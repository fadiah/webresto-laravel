<!DOCTYPE html>
<html>
<head>
	<title>Doy's Resto</title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="/asset/css/style.css">

</head>
<body>

		 <nav class="navbar navbar-expand-lg navbar-light" style="background-color: black;">
		      <img src="{{ asset('/asset/logoresto.png') }}" width="90px" height="90px">
		   		<div class="container">
		        <a class="navbar-brand font-weight-bold" href="restoran.html" style="color: white;font-family: Verdana, Geneva, Tahoma, sans-serif;">Doy's Resto ||</a>
		       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" style="background-color: red;">
			    <span class="navbar-toggler-icon"></span>
			  </button>

		        <div class="collapse navbar-collapse" id="navbarSupportedContent">
		          <ul class="navbar-nav ml-auto mr-4">
		            <li class="nav-item active">
		              <a class="nav-link font-weight-bold" href="restoran.html" style="color: white;">Home<span class="sr-only">(current)</span></a>
		            </li>
		            <li class="nav-item active">
		              <a class="nav-link font-weight-bold" href="#" style="color: white;">About<span class="sr-only">(current)</span></a>
		            </li>
		          </ul>
		          <div class="form-inline">
		            <input class="input-cari form-control mr-sm-2" type="text" placeholder="search">
		            <button class="tombol-cari btn btn-outline-black my-2 my-sm-0" style="color: white;background-color: red">Search</button>
		          </div>
		          <div class="icon mt-2">
		            <h5>
		              <i class="fas fa-cart-plus ml-3 mr-3" data-toggle="tooltip" title="Keranjang Belanja"></i>
		              <i class="fas fa-envelope mr-3" data-toggle="tooltip" title="Pesanan"></i>
		              <i class="fas fa-bell mr-3" data-toggle="tooltip" title="Notifikasi"></i>
		            </h5>
		          </div>
              <a href="/admin">
              <div class="d-flex align-items-center justify-content-center p-2 p-md-3 py-lg-4 mb-2" style="font-size: 1em;color: red;">
                <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-person-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"></path>
              </svg>
              </div>
              </a>
		        </div>
		      </div> 
    	</nav>

    <div class="row mt-5 no-gutters">
      <div class="col-md-2 bg-light">
        <ul class="list-group list-group-flush p-2 pt-4">
          <li class="list-group-item" style="background-color: black;color: white;"> <i class="fas fa-list"></i> OTHER</li>
          <li class="list-group-item"> <i class="fas fa-angle-right"></i> <a style="color: black" href="#"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-cart2" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M0 2.5A.5.5 0 0 1 .5 2H2a.5.5 0 0 1 .485.379L2.89 4H14.5a.5.5 0 0 1 .485.621l-1.5 6A.5.5 0 0 1 13 11H4a.5.5 0 0 1-.485-.379L1.61 3H.5a.5.5 0 0 1-.5-.5zM3.14 5l1.25 5h8.22l1.25-5H3.14zM5 13a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0zm9-1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0z"/>
          </svg> Order </a></li>
          <li class="list-group-item"> <i class="fas fa-angle-right"></i><a  style="color: black" href="#"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-list-stars" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M5 11.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5z"/>
            <path d="M2.242 2.194a.27.27 0 0 1 .516 0l.162.53c.035.115.14.194.258.194h.551c.259 0 .37.333.164.493l-.468.363a.277.277 0 0 0-.094.3l.173.569c.078.256-.213.462-.423.3l-.417-.324a.267.267 0 0 0-.328 0l-.417.323c-.21.163-.5-.043-.423-.299l.173-.57a.277.277 0 0 0-.094-.299l-.468-.363c-.206-.16-.095-.493.164-.493h.55a.271.271 0 0 0 .259-.194l.162-.53zm0 4a.27.27 0 0 1 .516 0l.162.53c.035.115.14.194.258.194h.551c.259 0 .37.333.164.493l-.468.363a.277.277 0 0 0-.094.3l.173.569c.078.255-.213.462-.423.3l-.417-.324a.267.267 0 0 0-.328 0l-.417.323c-.21.163-.5-.043-.423-.299l.173-.57a.277.277 0 0 0-.094-.299l-.468-.363c-.206-.16-.095-.493.164-.493h.55a.271.271 0 0 0 .259-.194l.162-.53zm0 4a.27.27 0 0 1 .516 0l.162.53c.035.115.14.194.258.194h.551c.259 0 .37.333.164.493l-.468.363a.277.277 0 0 0-.094.3l.173.569c.078.255-.213.462-.423.3l-.417-.324a.267.267 0 0 0-.328 0l-.417.323c-.21.163-.5-.043-.423-.299l.173-.57a.277.277 0 0 0-.094-.299l-.468-.363c-.206-.16-.095-.493.164-.493h.55a.271.271 0 0 0 .259-.194l.162-.53z"/>
          </svg> Me n u </a></li>
        </ul>
      </div>   


      <div class="col-md-10">
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img src="{{ asset('/asset/s1.jpg') }}" class="d-block w-100" alt="..." height="700">
            </div>
            <div class="carousel-item">
              <img src="{{ asset('/asset/s2.jpg') }}" class="d-block w-100" alt="..." height="700">
            </div>
            <div class="carousel-item">
              <img src="{{ asset('/asset/s3.jpg') }}" class="d-block w-100" alt="..." height="700">
            </div>
            
          </div>
          <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>

   </div>
</div>






	<br><br><br>
	<marquee style="font-family: times-new-roman;font-size: 36px"><b>MENU FAVORITE</b></marquee>
	<br><br>
	




  <div class="kolom-order">
    <h2>Jumlah Menu</h2>
    <h3></h3>
  </div>



  <div class="daftar-menu"></div>
	


    
  <br><br><br>

 
  
    <footer style="display: inline-flex;">
            <h3>Doy's Resto</h3>
            <div class="contact">
                <p class="text-call">Call: 1888 000</p>
                <p class="text-email">Email: doysrest029@gmail.com</p>
            </div>
            <p class="kebijakan">
                Kebijakan Privasi |
                Syarat dan Ketentuan |
                TM & &copy 2020 Doy's Resto Corporation. Used Under License. All rights reserved
            </p>
        </footer>

        

	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
	<script src="{{ asset('asset/js/menu.js') }}"></script>
  <script type="text/javascript" src="{{ asset('asset/js/data.js') }}"></script>
</body>

	
</html>