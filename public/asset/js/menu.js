const menu = [
				{
					gambarMakanan: "asset/menu2.png",
					namaMakanan: "Family Combo",
					hargaMakanan: 140000,
				 	deskripsiMakanan: "Paket Keluarga"
				}, 
				{
					gambarMakanan: "asset/menu4.png",
					namaMakanan: "Chicken Burger",
					hargaMakanan: 30000,
					deskripsiMakanan: "Burger Ayam"
				}, 
				{
					gambarMakanan: "asset/menu6.png",
					namaMakanan: "Friends Combo",
					hargaMakanan: 50000,
					deskripsiMakanan: "Paket Teman"
				},
				{
					gambarMakanan: "asset/menu8.png",
					namaMakanan: "Strawberry Iced",
					hargaMakanan: 25000,
					deskripsiMakanan: "Ice Strawberry"
				},
				{
					gambarMakanan: "asset/menu10.png",
					namaMakanan: "Pom Pom",
					hargaMakanan: 25000,
					deskripsiMakanan: "Pom Pom"
				},
				{
					gambarMakanan: "asset/menu12.png",
					namaMakanan: "Super Breakfast",
					hargaMakanan: 30000,
					deskripsiMakanan: "Paket Sarapan"
				},
				{
					gambarMakanan: "asset/menu1.png",
					namaMakanan: "9 pcs Bucket",
					hargaMakanan: 140000,
					deskripsiMakanan: "Paket Ayam"
				},
				{
					gambarMakanan: "asset/menu3.png",
					namaMakanan: "Chicken Wings",
					hargaMakanan: 120000,
					deskripsiMakanan: "Paket Sayap Ayam"
				},
				{
					gambarMakanan: "asset/menu5.png",
					namaMakanan: "Young Combo",
					hargaMakanan: 30000,
					deskripsiMakanan: "Paket Remaja"
				},
				{
					gambarMakanan: "asset/menu7.png",
					namaMakanan: "Chocolate Iced",
					hargaMakanan: 25000,
					deskripsiMakanan: "Ice Coklat"
				},
				{
					gambarMakanan: "asset/menu9.png",
					namaMakanan: "Lemon Tea Iced",
					hargaMakanan: 25000,
					deskripsiMakanan: "Ice Lemon"
				},
				{
					gambarMakanan: "asset/menu11.png",
					namaMakanan: "Scramble Egg",
					hargaMakanan: 25000,
					deskripsiMakanan: "Telur Acak"
				}
			];


const jumlahMENU = (array) => {
  const jmlItemUnsur = document.querySelector('.kolom-order h3');
  const jumlahItem = array.reduce((accumulator) => {
    return accumulator + 1;
  }, 0);
  jmlItemUnsur.innerHTML = jumlahItem;
}



const callbackMap = (item, index)=>{
	const element = document.querySelector('.daftar-menu');


	element.innerHTML += `<div class="container" style="margin-right:350px;">
						        <div class="row" style="float: left;">
						            <div class="col-sm m-2" >
						                <div class="card m-3" style="width: 15rem;">
						                    <img src="${item.gambarMakanan}" class="card-img-top" alt="...">
						                    <div class="card-body">
						                        <h4 class="card-title">${item.namaMakanan}</h4>
						                        <h5>Rp.${item.hargaMakanan}</h5><br>
						                        <p class="card-text" style="text-align:center;">${item.deskripsiMakanan}</b></p><br>
						                        <button class="btn btn-danger" style="background-color:red; color: white;">Buy Now</button>
						                    </div>
						                </div>
						            </div>
						        </div>
						     </div>`
}

menu.map(callbackMap);
jumlahMENU(menu);


const buttonElement	= document.querySelector('.tombol-cari');

buttonElement.addEventListener('click',()=>{						
	const hasilPencarian = menu.filter((item, index)=>{
								const inputElement	= document.querySelector('.input-cari');
								const namaItem		= item.namaMakanan.toLowerCase();
								const keyword 		= inputElement.value.toLowerCase();
		
								return namaItem.includes(keyword);
	})

	document.querySelector('.daftar-menu').innerHTML = '';

	hasilPencarian.map(callbackMap);
	jumlahMENU(hasilPencarian);
});

